﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPI_1_lab
{
    class Program
    {
        static int[] FunkSort(int[] mass,int n)
        {
            int b;
            for (int j = 0; j < n; j++)
                for (int i = 0; i < n; i++)
                {
                    if (mass[j] < mass[i])
                    {
                        b = mass[j];
                        mass[j] = mass[i];
                        mass[i] = b;
                    }
                }
            Console.WriteLine("Massiv vidsortovano!");
            return mass;
        }
        static void Main(string[] args)
        {
            int n;
            Console.WriteLine("Vvedit rozmir masivu: ");
            n = Int32.Parse(Console.ReadLine());
            int[] mas = new int[n];
            Random rand = new Random();
            for (int i = 0; i < n; i++)
            {
                mas[i] = rand.Next(0,99);
                Console.WriteLine("Element[{0}] = {1}",i+1,mas[i]);
            }
            mas = FunkSort(mas,n);
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Element[{0}] = {1}", i + 1, mas[i]);
            }
            Console.ReadKey();
        }
    }
}
