﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPI_1_lab_3
{
    class Program
    {
        static void MyFunk(int[][] mas, int[] m, int n)
        {
            int i, j, r=0, s=0, k=0, u=-1;
            Console.WriteLine("Rozrahunok sum po ryadkam!");
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m[i]; j++)
                {
                    r += mas[i][j];
                }
                Console.WriteLine("Suma {0} ryadka: {1}",i+1,r);
                r = 0;
            }
            Console.WriteLine("Rozrahunok sum po stolbikam!");
            do
            {
                u++;
                for (i = 0; i < n; i++)
                {
                    for (j = 0; j < m[i]; j++)
                    {
                        if (j == k)
                        {
                            s += mas[i][j];
                        }
                    }
                }
                Console.WriteLine("Suma {0} stolbika: {1}",k+1,s);
                k++;
                s = 0;
            }    
            while(u<n-1);
        }
        static void Main(string[] args)
        {
            int n,i,j;
            Console.Write("Vvedit kilkist ryadkiv v masivi: ");
            n = Int32.Parse(Console.ReadLine());
            int[][] mas = new int[n][];
            int[] m = new int[n];
            Random Rand = new Random();
            for (i = 0; i < n; i++)
            {
                Console.Write("Vvedit kilkist elmentiv v ryadku {0}: ",i+1);
                m[i]=Int32.Parse(Console.ReadLine());
                mas[i]=new int[m[i]];
                
            }
            for (i = 0; i < n; i++)
            {
                for (j = 0; j < m[i]; j++)
                {
                    mas[i][j] = Rand.Next(0, 99);
                    Console.Write(mas[i][j]+"   ");
                }
                Console.WriteLine();
            }
            MyFunk(mas,m,n);
            Console.ReadKey();
        }
    }
}
