﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OPI_1_lab_2
{
    class Program
    {
        static int[,] MyFunk(int[,] mas1, int[,] mas2, int m, int n, int x, int y)
        {
            int[,] result = new int[m, y];
            int i, j, k;
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < y; j++)
                {
                    for (k = 0; k < x; k++)
                    {
                        result[i,j] = mas1[i,k] * mas2[k,j];
                    }
                }
            }
            Console.WriteLine("Matrici peremnojeni!");
            return result;
        }
        static void Main(string[] args)
        {
            int m, n, x, y, i, j;
            Console.Write("Vvedit rozmiri pershoi matrici m=");
            m = Int32.Parse(Console.ReadLine());
            Console.Write("Vvedit rozmir pershoi matrici n=");
            n = Int32.Parse(Console.ReadLine()); 
            Console.Write("Vvedit rozmir drugoi matrici x=");
            x = Int32.Parse(Console.ReadLine()); 
            Console.Write("Vvedit rozmir drugoi matrici y=");
            y = Int32.Parse(Console.ReadLine());
            int[,] mas1 = new int[m, n];
            int[,] mas2 = new int[x, y];
            int[,] result = new int[m, y];
            Random Rand = new Random();
            Console.WriteLine("Matrica 1:");
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < n; j++)
                {
                    mas1[i,j] = Rand.Next(0,99);
                    Console.Write(mas1[i,j]+" ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Matrica 2:");
            for (i = 0; i < x; i++)
            {
                for (j = 0; j < y; j++)
                {
                    mas2[i, j] = Rand.Next(0, 99);
                    Console.Write(mas2[i,j]+" ");
                }
                Console.WriteLine();
            }
            result = MyFunk(mas1,mas2,m,n,x,y);
            Console.WriteLine("Matrica 3:");
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < y; j++)
                {
                    Console.Write(result[i,j]+" ");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
